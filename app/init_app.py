import os as _os
from flask import Flask, jsonify
from flask_cors import CORS
from flask import current_app
# from flask_bcrypt import Bcrypt

import sys
from flask_jwt_extended import JWTManager


def register_blueprints(app):
    from api import api_blueprint
    app.register_blueprint(api_blueprint)    


def create_app(config_filename=None):
    _app = Flask(__name__, instance_relative_config=True)
    with _app.app_context():
        print "Creating web app",current_app.name
        cors = CORS(_app, resources={r'/*': {"origins": "*"}})
        sys.path.append(_app.instance_path)
        _app.config.from_pyfile(config_filename)
        from config import app_config
        config = app_config[_os.environ.get('APP_SETTINGS', app_config.development)]
        _app.config.from_object(config)
        # flaskbcrypt = Bcrypt()
        # flaskbcrypt.init_app(_app)
        register_blueprints(_app)
        # _app.app_context().push() # AssertionError: Popped wrong app context.  
        return _app


app = create_app(config_filename="config.py")
jwt = JWTManager(app)


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        "status": "Fail",
        "message": "Missing Authorization Header"
    }), 401


