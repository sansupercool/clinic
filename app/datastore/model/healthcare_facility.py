""" 
This module contains peewee model for Medical Center.
http://docs.peewee-orm.com/en/latest/index.html
https://medium.com/@prabhath_kiran/introduction-to-peewee-and-relations-1c72af26e1b9
"""

import peewee
from app.datastore.model.user import User
from app.datastore.core import db as _db

# _db = peewee.MySQLDatabase('clinic_backend', user='pi', host='ceres.local', port=3306, password="pimysql")


class HealthCareFacility(peewee.Model):
	name = peewee.CharField(max_length=90)
	owner = peewee.ForeignKeyField(User)
	facility_type = peewee.CharField(max_length=15)
	gov_registration = peewee.CharField(max_length=25)
	beds = peewee.IntegerField()
	otfacility = peewee.BooleanField(null=True)
	noofot = peewee.IntegerField()
	addr_line1 = peewee.CharField(max_length=40)
  	addr_line2 = peewee.CharField(max_length=40)    
  	street = peewee.CharField(max_length=40)
  	city  = peewee.CharField(max_length=60)
  	province = peewee.CharField(max_length=120) # eg: state : Punjab
  	country = peewee.CharField(max_length=160)
  	map_url = peewee.CharField(max_length=250)
  	logo_filename = peewee.CharField(max_length=5)
  	logo_url = peewee.CharField(max_length=250)
  	note = peewee.TextField() # any additional info regarding the medical facility.

  	class Meta:
  		database = _db


class HealthCareFacilityContact(peewee.Model):
	facility_id = peewee.ForeignKeyField(HealthCareFacility)
	contactnumber = peewee.CharField()
	email = peewee.CharField(max_length=120)


  	class Meta:
  		database = _db
