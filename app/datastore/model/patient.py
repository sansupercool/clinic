""" 
This module contains peewee model for Patient.
http://docs.peewee-orm.com/en/latest/index.html
https://medium.com/@prabhath_kiran/introduction-to-peewee-and-relations-1c72af26e1b9
"""

import peewee
from app.datastore.core import get_db as _db


class Patient(peewee.Model):
	""" Patinet table """

	name = peewee.CharField(max_length=90)
	towncity = peewee.CharField(max_length=200)
	contactnumber = peewee.CharField(max_length=20)
	bloodgroup = peewee.CharField(max_length=4)
	govid = peewee.CharField(max_length=20) # TODO: get rid of this
	govid_type = peewee.CharField(max_length=20)
	govtid_number = peewee.CharField(max_length=20, null=True)
	govtid_type = peewee.CharField(max_length=15, null=True)
	age = peewee.SmallIntegerField(default=0)
	dob = peewee.DateField()
	gender = peewee.CharField(max_length=9)
	email = peewee.CharField(max_length=1024, null=True)
	postalAddress = peewee.TextField() ## https://www.quora.com/What-is-the-data-type-for-postal-address-in-SQL-database

	@classmethod
	def get_patients(cls):
		return [patient.name for patient in cls.select()]

	def __repr__(self):
		return "<Patient Id: {}>".format(self.id)

	class Meta:
		database = _db()

class Relative(peewee.Model):
	""" Patient Relative record """
	patient = peewee.ForeignKeyField(Patient, 
		related_name="patient")
	relation = peewee.CharField(max_length=10)
	name = peewee.CharField(max_length=90)

	class Meta:
		database = _db()

	@classmethod
	def get_relatives(cls):
		return [relative.name for relative in cls.select()]


# the below table continues to increment on each visit of new or old patient by frontdesk.

class PatientAttendOnVisit(peewee.Model):
	""" Attendence or visit record """

	patient = peewee.ForeignKeyField(Patient, 
		related_name="patient")
	dateTimeofvisit = peewee.DateTimeField()
	attendtype = peewee.CharField(max_length=12) # walkin or appointment
	department = peewee.CharField(max_length=9) # medicine or dental
	drug_alergies = peewee.CharField(max_length=40)

	class Meta:
		database = _db()

# the below table continues to increment on each patient visit by doctor

class PatientExamination(peewee.Model):
	""" 
	PatientExamination table stores what patient reports on each visit 
	to doctor like symptoms, diagnosis and presciption
	"""

	chiefcomplaint = peewee.TextField()
	symptoms = peewee.TextField()
	diagnosis = peewee.TextField()
	treatment = peewee.TextField()
	prescription = peewee.TextField()
	refer_to = peewee.CharField(max_length=40)
	dateTimeofvisit = peewee.ForeignKeyField(PatientAttendOnVisit)

	class Meta:
		database = _db()

class PatientVitals(peewee.Model):
	""" Patient Vitals on each visit
	"""
	patient_id = peewee.ForeignKeyField(Patient)
	height = peewee.FloatField()
	weight = peewee.FloatField()
	temperature = peewee.FloatField()
	bloodpressure = peewee.CharField(max_length=10)

	class Meta:
		database = _db()



