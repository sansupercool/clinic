import peewee
from app.datastore.core import db as _db
import datetime
from werkzeug.security import check_password_hash
# from deploy import ldap_connect
    

class User(peewee.Model):
    """ User Table """

    username = peewee.CharField(max_length=60, unique=True)
    email = peewee.CharField(max_length=60)
    authenticated = peewee.BooleanField(default=False)
    is_admin = peewee.BooleanField(default=False)
    registered_on = peewee.DateTimeField(constraints=[peewee.SQL('DEFAULT CURRENT_TIMESTAMP')])

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return True

    def is_anonymous(self):
        return True

    def is_guest(self):
        return True

    def get_id(self):
        return unicode(self.id)

    @classmethod
    def get_user(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def get_users(cls):
        return [user.username for user in cls.select()]

    def verify_password(self, password):
        """ Check if hashed password matches actual password
        :param
            password (string)
        """
        # ldap_obj = ldap_connect.LDAP()
        # if not ldap_obj.authenticate(self.username, password):
        #     return False
        # else:
        #     ldap_obj.unbind()
        # return True
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User: {}>".format(self.username)

    class Meta:
        database = _db


class UserProfile(peewee.Model):
    first_name = peewee.CharField(max_length=60, default="")
    middle_name = peewee.CharField(max_length=60, default="")
    last_name = peewee.CharField(max_length=60, default="")
    dob = peewee.DateField()
    gender = peewee.CharField(max_length=9)
    username = peewee.ForeignKeyField(User)

    class Meta:
        database = _db


class Department(peewee.Model):
    """ Create dept. table
    """

    name = peewee.CharField(max_length=256)
    description = peewee.TextField()

    def __repr__(self):
        return "<Department: {}>".format(self.name)

    class Meta:
        database = _db


class Role(peewee.Model):
    """ Create role table
    """
    name = peewee.CharField(max_length=256)
    description = peewee.TextField()

    def __repr__(self):
        return "<Role: {}".format(self.name)

    class Meta:
        database = _db


class UserRoles(peewee.Model):
    user = peewee.ForeignKeyField(User, related_name="roles")
    role = peewee.ForeignKeyField(Role, related_name="users")
    name = property(lambda self: self.role.name)
    description = property(lambda self: self.role.description)

    class Meta:
        database = _db


class AuthUser(peewee.Model):
    user = peewee.ForeignKeyField(User)
    hash = peewee.CharField(max_length=300)

    class Meta:
        database = _db

