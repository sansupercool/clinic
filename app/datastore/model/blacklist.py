import peewee
from app.datastore.core import db as _db


class BlacklistToken(peewee.Model):
    """
    Token model for storing Blacklisted tokens
    """
    token = peewee.TextField()
    blacklisted_on = peewee.DateTimeField(constraints=[peewee.SQL('DEFAULT CURRENT_TIMESTAMP')])

    def __repr__(self):
        return "<id: token: {}>".format(self.token)

    @staticmethod
    def check_blacklisted(auth_token):
        """
        check if toke is blacklisted or not.

        :param str auth_token:
        :return: True if token is black listed else return False
        :rtype: bool
        """
        return auth_token in [auth.token for auth in BlacklistToken.select()]

    class Meta:
        database = _db
