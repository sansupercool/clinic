
import peewee
import os

# https://stackoverflow.com/questions/34038185/peewee-mysql-server-has-gone-away

from flask import g, current_app
app = current_app
cfg = current_app.config

dbName = 'clinic_backend'


def connect_db():
    """ Connects to the specific database. """

    return peewee.MySQLDatabase(dbName, 
    user=cfg['DB_USER'], 
    host=cfg['DB_HOST'], 
    port=3306, 
    password=cfg['DB_PWD']
    )

def get_db():
    """ Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'db'):
        db = connect_db()
    return db

db = get_db()

