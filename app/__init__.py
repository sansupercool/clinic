from flask import jsonify
from flask.cli import FlaskGroup
from init_app import create_app, app

# while running in shell for testing do not forget to export FLASK_APP=start_backend.py
web_app = app

from app.datastore.core import db, connect_db
from app.datastore.model import blacklist, user, patient, healthcare_facility


@app.before_request
def before_request():
	connect_db()

@app.teardown_appcontext
def close_database(error):
	db.close()

web_app.config.SWAGGER_UI_OPERATION_ID = True
web_app.config.SWAGGER_UI_REQUEST_DURATION = True

web_app.config['JWT_BLACKLIST_ENABLES'] = True
web_app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
web_app.config['JWT_SECRET_KEY'] = 'super-secret'  # temporary
web_app.config["JWT_ACCESS_COOKIE_NAME"]="user_access_token"
# web_app.config['JWT_TOKEN_LOCATION'] = ['cookies']  # setting cookie is disabled and using Header instead
web_app.config['JWT_ACCESS_COOKIE_PATH'] = '/api/'
web_app.config['JWT_REFRESH_COOKIE_PATH'] = '/token/refresh'
web_app.config['JWT_COOKIE_CSRF_PROTECT'] = True  # Disable CSRF protection 


@web_app.shell_context_processor
def make_shell_context():
	return dict(app=web_app, db=db, User=user.User, Role=user.Role, AuthUser=user.AuthUser,
		Department=user.Department, UserRoles=user.UserRoles, Relative=patient.Relative,
		 BlacklistToken=blacklist.BlacklistToken, Patient=patient.Patient, 
		 PatientAttendOnVisit=patient.PatientAttendOnVisit, PatientCheckup=patient.PatientExamination)

cli = FlaskGroup(create_app=create_app)

@web_app.cli.command()
def init_db():
	# initial db setup, create table for each mode if it does not exists
	# use the underlying peewee database object instead of the
	# flask-peewee database wrapper:
	db.create_tables(
		[user.User, user.Role, user.UserRoles, user.Department, 
		blacklist.BlacklistToken, user.AuthUser,
		patient.Patient, patient.Relative, patient.PatientVitals,
		patient.PatientAttendOnVisit, healthcare_facility.HealthCareFacility,
		patient.PatientExamination, healthcare_facility.HealthCareFacilityContact],
		safe=True)


