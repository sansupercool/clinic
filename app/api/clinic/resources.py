
from flask import request
from flask_jwt_extended import jwt_required
from app.api.clinic import controller
from flask_restplus import Resource
from app.api.auth import auth_helper

from ..dto import PatientDto, SearchPatients, PatientProfile
from ..routes import clinic_api_ns


visiting_patient = PatientDto.patient
query = SearchPatients.query
patient_profile = PatientProfile.profile


@clinic_api_ns.route("/patient/<string:name>", methods=['GET'])
@clinic_api_ns.route("/patient/add/", methods=["POST"])
class Patient(Resource):
	""" Patient endpoint."""
	@clinic_api_ns.expect(visiting_patient, validate=True)
	def post(self):
		# TODO: add a patient.
		data = request.json
		print "Data: ", data
		return controller.add_patient_visit(data=data)
	
	def get(self, name=None):
		# TODO: get a patient record
		print "NAme: ", name
		return controller.get_patient(name)


@clinic_api_ns.route("/patients_relatives/names")
class Names(Resource):
	# @clinic_api_ns.expect(query, validate=True)
	def get(self):
		# query = request.args.get('q')# or request.json['q']
		return controller.get_names()


@clinic_api_ns.route("/names")
class QueryNames(Resource):
	def get(self):
		query_text = request.args.get("q")
		return controller.query_names(query_text)


@clinic_api_ns.route("/relatives/patient/<string:name>")
class Relatives(Resource):
	def get(self, name=None):
		return controller.getRelatives(name)


@clinic_api_ns.route("/patient/profile")
class PatientProfile(Resource):
	@clinic_api_ns.expect(patient_profile, validate=False)
	def get(self):
		patient = request.args.get('patient')
		relative = request.args.get('relative')
		relationType = request.args.get('relationType')
		contactNumber = request.args.get('contactNumber')
		townCity = request.args.get('townCity')
		return controller.get_patient_profile(patient, relative, relationType, contactNumber, townCity)



