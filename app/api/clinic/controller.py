import peewee
from peewee import fn
from app.datastore.model import patient as _patient
from app.datastore.core import db as _db
from flask_security import datastore as _datastore

patient_datastore = _datastore.PeeweeUserDatastore(_db, _patient.Patient, _patient.Relative, _patient.PatientAttendOnVisit)


def add_patient_visit(data=None):
	"""
	Adds new visit to clinic of patient 
	for new or follow up patient.
	"""
	if not data:
		raise ValueError("Please pass the user info.")
	print "Patient Name: ", data['name']
	patientName = data["name"]
	doctor = data["doctor"]
	patient = _patient.Patient.create(
		name=patientName,
		towncity=data["townCity"],
		contactnumber=data["contactnumber"],
		age=data["age"],
		gender=data["gender"],
		email=data["email"],
		postalAddress=data["postalAddress"])

	relative = _patient.Relative.create(
		patient=patient,
		relation=data["relation"],
		name=data["relativeName"])

	attendence = _patient.PatientAttendOnVisit.create(
		patient=patient,
		dateTimeofvisit=data["dateTimeofvisit"],
		attendtype=data["attendtype"],
		department=data["department"]
		)

	if patient and attendence:
		return {"status": "success", 
		"message": "Patient {} visit successfully added to queue for Dr. {}".format(
			patientName,
			doctor )}, 200
	else:
		return {"status": "fail", "message": "Failed to add user attendence of current visit."}, 400

def get_names():
	""" Get the patient with the name provided
	"""
	patients = _patient.Patient.get_patients()
	relatives = _patient.Relative.get_relatives()
	# patients = [p.name for p in _patient.Patient.select().where(fn.LOWER(fn.SUBSTR(_patient.Patient.name, 1, 1)) == query)]
	# patients = patients + ['new']
	response_object = {
		"status": "success",
		"names": patients + relatives 
	}
	return response_object, 201


def query_names(query):
	patients = [p.name for p in _patient.Patient.select().where(fn.LOWER(fn.SUBSTR(_patient.Patient.name, 1, 1)) == query)]
	response_object = {
		"status": "success",
		"names": patients 
	}
	return patients, 201


def get_patient(name):
	patient = [pat.name for pat in _patient.Patient.select().where(name=name)]
	return {"status": "success", "patients": patient}, 201


def getRelatives(name=None):
	relatives = [relative.name for relative in (
		_patient.Relative.select(
			_patient.Relative.name).join(
				_patient.Patient
					).where(
						_patient.Patient.name == name
					)
			)
		]
	return {"status": "success", "relatives": relatives}, 201


def get_patient_profile(patient, relative, relationType=None, contactnumber=None, towncity=None):
	"""	Provides patient profile information

	:param str patient: name of the patient
	:param str relative: name of the relative
	
	"""
	# First lets get the id

	patients = _patient.Patient.select().join(
		_patient.Relative
		).where(
		_patient.Patient.name == patient, 
		_patient.Relative.name == relative
		)
	
	if relationType:
		patients = patients.where(_patient.Relative.relation == relationType)

	if towncity:
		patients = patients.where(_patient.Patient.towncity == towncity)

	if contactnumber:
		patients = patients.where(_patient.Patient.contactnumber == contactnumber)

		
	profile = {}

	if len(patients) > 1:
		msg = 'More than 1 patient found, please select more fields or continue typing name of relative.'
		return {
		"status": 'success', 
		"message": msg, 
		'profile': profile 
		}, 201

	for sel_patient in patients:
		profile['name'] = sel_patient.name
		profile['id'] = sel_patient.id
		profile['age'] = sel_patient.age
		profile['gender'] = sel_patient.gender
		profile['towncity'] = sel_patient.towncity
		profile['contactNumber'] = sel_patient.contactnumber
		profile['email'] = sel_patient.email
		profile['address'] = sel_patient.postalAddress

		relation = [rel.relation for rel in _patient.Relative.select(
			_patient.Relative.relation
			).where(
			_patient.Relative.id == sel_patient.id
			)]
		if relation:
			profile['relation'] = relation[0]



	return {"status": 'success', 'message':'retrieved patient profile.', 'profile': profile}, 201 


