from flask import jsonify
from app.datastore.model import user as _user
from app.datastore.model.blacklist import BlacklistToken
from flask_jwt_extended import (
    create_access_token,
    decode_token,
    set_access_cookies,
    unset_jwt_cookies
    )
from flask_jwt_extended.config import config
import datetime


def save_token(token):
    try:

        # insert the token
        BlacklistToken.insert(token=token).execute()
        # TODO:  set authenticated to 0 in Users table.
        response_object = jsonify({
            "status": "success",
            "message": "Successfully logged out."
        })
        # unset_jwt_cookies(response_object)
        # status code is 200
        return response_object
    except Exception as er:
        response_object = {"status": "fail", "message": er}
        return response_object, 500


def decode_auth_token(auth_token):
    """
    Decode the auth token
    :param str auth_token:
    :return:
    """
    payload = decode_token(auth_token)
    is_blacklisted_token = BlacklistToken.check_blacklisted(auth_token)
    if is_blacklisted_token:
        return "Token blacklisted. Please log in again."
    else:
        return payload['exp']


def encode_auth_token(user, fresh=True, expires_delta=None):
    """
    Generates Auth token

    :param str user:
    :return:
    """
    expires_delta = expires_delta or datetime.timedelta(days=1, seconds=5)
    access_token = create_access_token(identity=user, fresh=fresh, expires_delta=expires_delta)
    return access_token


class Auth(object):

    @staticmethod
    def login_user(data, client=None):
        try:
            # fetch the user data
            try:
                user = _user.User.get(username=data.get('username'))
            except Exception:
                user = None
            if user and user.verify_password(data.get("password")):
                ed = datetime.timedelta(weeks=5215) if client == "bfx_scm/flow" else None
                auth_token = encode_auth_token(user.username, expires_delta=ed)
                if auth_token:
                    # set authenticated to 1 in Users table.
                    upd = _user.User.update(authenticated=1).where(_user.User.username == user.username)
                    upd.execute()
                    response_object = jsonify({
                        "status": "success",
                        "message": "Successfully Logged in.",
                        "Authorization": auth_token.decode()
                    })
                    token = auth_token.decode()

                    # my research concluded that tokens are preferable stored in db rather than storing in cookie
                    # but still I am leaving the working code commented if you want to make use of cookie approach

                    # response_object.set_cookie(
                    #     config.access_cookie_name,
                    #     value=auth_token,
                    #     max_age=config.cookie_max_age,
                    #     secure=config.cookie_secure,
                    #     httponly=True,
                    #     path=config.access_cookie_path,
                    #     samesite=config.cookie_samesite)

                    # either above or below one line

                    # set_access_cookies(response_object, auth_token)
                    return response_object
            else:
                response_object = {
                    "status": "fail",
                    "message": "username or password incorrect or does not match."
                }
                return response_object, 401
        except Exception as er:
            print "Error occurred: ", er
            response_object = {
                "status": "fail",
                "message": "Try Again."
            }
            return response_object, 500

    @staticmethod
    def has_logout(token):
        return BlacklistToken.check_blacklisted(token)

    @staticmethod
    def log_out(data):
        if data:
            auth_token = data.split(" ")[1]
        else:
            auth_token = ""

        if auth_token:
            resp = decode_auth_token(auth_token)
            username = decode_token(auth_token)["identity"]
            if not isinstance(resp, str):
                # mark authenticated 0 if log out
                upd = _user.User.update(authenticated=0).where(_user.User.username == username)
                upd.execute()
                # mark the token blacklisted
                return save_token(token=auth_token)
            else:
                response_object = {
                    "status": "fail",
                    "message": resp
                }

                return response_object, 401
        else:
            response_object = {
                "status": "fail",
                "message": "provide a valid auth token."
            }
            return response_object, 403

