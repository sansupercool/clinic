""" This module contains Authentication related web api.
"""

from flask import request
from flask_restplus import Resource
from auth_helper import Auth
from ..routes import auth_api_ns
from ..dto import AuthDto


user_auth = AuthDto.user_auth

@auth_api_ns.route("/login")
@auth_api_ns.response(200, "Successfully logged in.")
@auth_api_ns.doc("Login to ple with credentials.")
class LoginUser(Resource):
    @auth_api_ns.expect(user_auth, validate=True)
    def post(self):
        """ Log in registered user."""
        post_data = request.json
        user_agent = request.headers.get("User-Agent")
        return Auth.login_user(data=post_data, client=user_agent)


@auth_api_ns.route("/logout")
class LogoutAPI(Resource):
    """ Logs out logged in user."""

    @auth_api_ns.doc("logout a user.")
    def post(self):
        # get auth token
        auth_header = request.headers.get("Authorization")
        return Auth.log_out(data=auth_header)

