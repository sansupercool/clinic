from flask_restplus import Resource, Api

from . import api_blueprint as _bp

api = Api(_bp, catch_all_404s=True, prefix="/api", version=0.1,
          title="Patient Health Care record management REST HTTP API's Gateway",
          descrition="REST API gateway for Patient Record Management.")

api.namespaces.pop(0)

user_api_ns = api.namespace("user", description="API for User management.")
auth_api_ns = api.namespace("auth", description="API for User authentication.")
clinic_api_ns = api.namespace("clinic", description="API for clinical record maintainence.") # TODO: rename to patient
