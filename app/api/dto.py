"""
This module acts as data transfer object that will be responsible for carrying
data between processes. see http://en.wikipedia.org/wiki/Data_transfer_object
"""

from flask_restplus import Resource, fields, Namespace
from routes import user_api_ns
from routes import auth_api_ns
from routes import clinic_api_ns


class AuthDto(object):
    user_auth = auth_api_ns.model("auth_details", {
        "username": fields.String(required=True, description="user username."),
        "password": fields.String(required=True, description="user password")
    })


class UserDto(object):
    user = user_api_ns.model('user', {
        "email": fields.String(required=False, description="user email address."),
        "username": fields.String(required=True, description="user's username."),
        "password": fields.String(required=True, description="user's password."),
    })

class PatientDto(object):
    patient = clinic_api_ns.model("patient", {
        "name": fields.String(required=True, description="name of the patient."),
        "townCity": fields.String(required=True, description="town or city the patient belongs to."),
        "contactnumber": fields.String(required=True, description="contact number of the patient."),
        "age": fields.Integer(required=False, description="age of the patient."),
        "gender": fields.String(required=False, description="sex of the patient"),
        "email": fields.String(required=False, description="email of the patient."),
        "postalAddress": fields.String(required=False, description="postal address of the patient."),
        "relation": fields.String(required=True, description="type of relation with the patient."),
        "relativeName": fields.String(required=True, description="name of parent or husband."),
        "dateTimeofvisit": fields.String(required=True, description="date and time of visit to clinic"),
        "attendtype": fields.String(required=False, description="whether the patient visiting by appointment or walkin"),
        "department": fields.String(required=True, description="department patient want to go to.")
        })

class PatientProfile(object):
    profile = clinic_api_ns.model("profile", {
        "patient": fields.String(required=True, description="name of the patient."),
        "relative": fields.String(required=True, description="name of parent or husband."),
        "relation": fields.String(required=True, description="type of relation with patient."),
        "contactnumber": fields.String(required=True, description="contact number of the patient."),
        "townCity": fields.String(required=True, description="town or city the patient belongs to."),        
        })

class SearchPatients(object):
    query = clinic_api_ns.model("query", {
        "q": fields.String(required=False, default="s", description="query matching patient name")
        })
