""" This module contains User related web api.
"""

from flask import request
from flask_jwt_extended import jwt_required
from flask_restplus import Resource
from app.api.auth import auth_helper

from ..dto import UserDto, AuthDto
from .user_service import save_new_user, get_all_users, authenticate, generate_token
from ..routes import user_api_ns


user_reg = UserDto.user
user_auth = AuthDto.user_auth


@user_api_ns.route("/register/")
@user_api_ns.response(201, "User successfully created.")
@user_api_ns.doc("Create a new user.")
class RegisterUser(Resource):
    @user_api_ns.expect(user_reg, validate=True)
    def post(self):
        """ Register a new user. """
        data = request.json
        return save_new_user(data=data)


@user_api_ns.route("/all")
@user_api_ns.response(201, "All Users listed successfully.")
@user_api_ns.doc("List all users.")
class ListAllUsers(Resource):
    @jwt_required
    def get(self):
        """ List all registered users. """
        auth_header = request.headers.get("Authorization")
        token = auth_header.split(" ")[1]
        if auth_helper.Auth.has_logout(token):
            response_object = {
                "status": "fail",
                "message": "You have logged out, please login again."
            }
            return response_object, 401
        else:
            return get_all_users()


@user_api_ns.route("/authenticate")
@user_api_ns.response(201, "Successfully authenticated registered user.")
class AuthenticateRegisteredUsers(Resource):
    @user_api_ns.expect(user_auth, validate=True)
    def post(self):
        """ Authenticated a added user. """
        data = request.json
        user = authenticate(data.get("username"), data.get("password"))
        if user:
            return generate_token(user)
        return {"status": "Fail", "message": "unable to authenticated user {}, possible invalid credentials.".format(
            data.get("username"))}, 201




