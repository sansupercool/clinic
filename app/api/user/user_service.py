
import datetime
import peewee
from app.datastore.model import user as _user
from app.datastore.core import db as _db
from flask_security import datastore as _flk_datastore
from app.api.auth import auth_helper


user_datastore = _flk_datastore.PeeweeUserDatastore(_db, _user.User, _user.Role, _user.UserRoles)


def generate_token(user):
    try:
        auth_token = auth_helper.encode_auth_token(user.username)
        response_object = {
            "status": "success",
            "message": "Successfully Registered",
            "Authorization": auth_token.decode()
        }
        return response_object, 201
    except Exception as er:
        response_object = {
            "status": "fail",
            "message": "{} error occurred. Please try again. ".format(er)
        }
        return response_object, 401


def save_new_user(data):
    """
    Save the new user if it doesnt exists.

    :param dict data:
    :return: json response object
    """
    if not data:
        raise ValueError("Please pass the user info.")
    try:
        _user.User.get(email=data["username"])
        response_object = {
            "status": "fail",
            "message": "User already exists."
        }
        return response_object, 409
    except peewee.DoesNotExist as er:
        user = user_datastore.create_user(
            email=data["email"],
            username=data["username"],
            registered_on=datetime.datetime.utcnow()
        )

        # user = authenticate(data.get('username'), data.get("password"))
        if user:
            response = generate_token(user)
            return response
        else:
            _user.User.delete().where(username=data["username"])
            return {"status": "Fail", "message": "Invalid username and password"}, 401


def authenticate(username, password):
    try:
        user = _user.User.get(username=username)
    except Exception:
        user = None
    if user and user.verify_password(password):
        pwd_hash = auth_helper.encode_auth_token(
            password,
            fresh=False,
            expires_delta=datetime.timedelta(weeks=5215)
        )
        # add password hash in auth table.
        _user.AuthUser.create(user_id=user.id, hash=pwd_hash)
        # mark user logged in
        upd = _user.User.update(authenticated=1).where(_user.User.username == user.username)
        upd.execute()
        return user


def get_all_users():
    users = _user.User.get_users()
    response_object = {
        "status": "success",
        "users": users
    }
    return response_object, 201

