

from flask import Blueprint

api_blueprint = Blueprint('api', __name__)

import routes

from .user import resources as _user_resources
from .auth import resources as _auth_resources
from .clinic import resources as _clinic_resources
