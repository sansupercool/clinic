import sys
from app import web_app

application = web_app

if __name__ == "__main__":
    web_app.debug = True if "-d" in sys.argv else False
    if "-t" in sys.argv:
        from flask_debugtoolbar import DebugToolbarExtension
        toolbar = DebugToolbarExtension(app)
    web_app.run(host='0.0.0.0', port=5060)
